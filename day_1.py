# To use Ray, you need to understand the following:
#  How Ray executes tasks asynchronously to achieve parallelism.
#  How Ray uses object IDs to represent immutable remote objects.

# To start Ray, start Python and run the following commands.
import ray
ray.init()

# Put and Get
x = "example"
ray.put(x)

x_id = ray.put(x)
ray.get(x_id) # "example"

# A very common use case of ray.get is to get a list of object IDs.
result_ids = [ray.put(i) for i in range(10)]
ray.get(result_ids)

# Asynchronous Computation in Ray

def add1(a, b):
    return a + b

@ray.remote
def add2(a, b):
    return a + b

x_id = add2.remote(1,2)
ray.get(x_id) # 3

# -----
import time

def f1():
    time.sleep(1)

@ray.remote
def f2():
    time.sleep(1)

# The following takes ten seconds.
[f1() for _ in range(10)]

# The following takes one second (assuming the system has at least ten CPUs).
ray.get([f2.remote() for _ in range(10)])

# a remote function can return multiple object IDs.
@ray.remote(num_return_vals=3)
def return_multiple():
    return 1,2,3

a_id, b_id, c_id = return_multiple.remote()

import numpy as np

@ray.remote
def generate_data():
    return np.random.normal(size=1000)

@ray.remote
def aggregate_data(x,y):
    return x+y

# Generate some random data. This launches 100 tasks that will be scheduled on
# various nodes. The resulting data will be distributed around the cluster.
data = [generate_data.remote() for _ in range(100)]

# Perform a tree reduce.
while len(data) > 1:
    data.append(aggregate_data.remote(data.pop(0), data.pop(0)))

# Fetch the result.
ray.get(data)

# Remote Functions Within Remote Functions
@ray.remote
def sub_experiment(i, j):
    # Run the jth sub-experiment for the ith experiment.
    return i + j

@ray.remote
def run_experiment(i):
    sub_results = []
    # Launch tasks to perform 10 sub-experiments in parallel.
    for j in range(10):
        sub_results.append(sub_experiment.remote(i, j))
    # Return the sum of the results of the sub-experiments.
    return sum(ray.get(sub_results))

results = [run_experiment.remote(i) for i in range(5)]
ray.get(results)











